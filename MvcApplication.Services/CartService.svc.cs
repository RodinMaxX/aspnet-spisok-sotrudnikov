﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Interfaces;

namespace MvcApplication.Services
{
    public class CartService : ICartService
    {
        protected readonly IUnitOfWork Context;

        public CartService(IUnitOfWork context)
        {
            Context = context;
        }

        public CartService()
            : this(new UnitOfWork())
        {
        }

        public string AddOrUpdate(int? accountId, IList<CartItemDataContract> cartItems)
        {
            // Загружаем из базы или создаём новый аккаунт для покупателя
            Account account;
            if (!accountId.HasValue)
            {
                account = new Account
                {
                    Login = "guest-" + Guid.NewGuid().ToString("N"), 
                    Password = "12345",
                };
                Context.Table<Account>().Add(account);
                Context.SaveChanges();
            }
            else account = Context.Table<Account>().Get(accountId.Value);

            // Создаём корзину покупок пользователю, если у него её нет
            if (account.LastCart == null) account.LastCart = new Cart
            {
                AccountId = account.Id,
                CartItems = new List<CartItem>(),
            };
            var oldCart = account.LastCart.CartItems.ToDictionary(x => x.ProductId);

            // Добавляем в корзину выбранные товары или обновляется количество
            foreach (var item in cartItems)
            {
                CartItem oldItem;
                if (oldCart.TryGetValue(item.ProductId, out oldItem))
                {
                    Context.Table<CartItem>().Get(oldItem.Id).Count = item.Count;
                    oldCart.Remove(oldItem.ProductId);
                }
                else account.LastCart.CartItems.Add(new CartItem(item));
            }

            // Удаляем товары, от которых отказался покупатель и сохраняем всё в базу
            foreach (var oldItem in oldCart.Values)
            {
                Context.Table<CartItem>().Delete(oldItem.Id);  
            } 
            Context.SaveChanges();
            return account.Login;
        }

        public List<CartItemDataContract> Filter(int accountId)
        {
            var account = Context.Table<Account>().Get(accountId);
            if (account.LastCart == null) return new List<CartItemDataContract>(0);
            return account.LastCart.CartItems.Select(x => new CartItemDataContract(x)).ToList();
        }

        public List<ProductDataContract> GetProducts(int accountId)
        {
            var account = Context.Table<Account>().Get(accountId);
            if (account.LastCart == null) return new List<ProductDataContract>(0);
            return account.LastCart.CartItems.Select(x => new ProductDataContract(x.Product)).ToList();
            
        }

        public void FinishBuy(int? accountId)
        {
            if (accountId.HasValue)
            {
                var account = Context.Table<Account>().Get(accountId.Value);
                if (account.LastCart != null)
                {
                    account.LastCart.IsSold = true;
                    account.LastCart = null;
                    Context.SaveChanges();
                }
            }
        }
    }
}
