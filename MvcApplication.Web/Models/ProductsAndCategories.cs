﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcApplication.DataContract;
using MvcApplication.Entities;

namespace MvcApplication.Models
{
    public class ProductsAndCategories
    {
        public IList<ProductDataContract> Products { get; private set; }
        public FilterModel Filter { get; set; }
        private readonly IDictionary<int, SelectListItem> _categories;
        private IDictionary<int, CartItem> _cartItems;
        
        public ProductsAndCategories(IList<ProductDataContract> products, IEnumerable<CategoryDataContract> categories)
        {
            Products = products;
            _categories = categories.ToDictionary(x => x.Id, x => new SelectListItem {Value = x.Id.ToString(), Text = x.Name});
        }

        public ProductsAndCategories()
        {
        }

        public Product Product
        {
            get { return (Products == null) || (Products.Count == 0) ? null : new Product(Products.Single()); }
            set { Products = new[] { new ProductDataContract(value) }; }
        }

        public ICollection<SelectListItem> Categories
        {
            get { return _categories.Values; }
        }

        public ICollection<CartItem> CartItems
        {
            get { return (_cartItems != null) ?_cartItems.Values : null; }
            set { _cartItems = value.ToDictionary(x => x.ProductId); }
        }


        public string GetCategoryName(int? id)
        {
            SelectListItem category;
            if (id.HasValue && _categories != null && _categories.TryGetValue(id.Value, out category))
            {
                return category.Text;
            }
            else return null;
        }

        public CartItem GetCartItem(int productId)
        {
            CartItem result;
            if (CartItems != null && _cartItems.TryGetValue(productId, out result)) return result;
            else return new CartItem {Count = 1};
        }
    }
}
