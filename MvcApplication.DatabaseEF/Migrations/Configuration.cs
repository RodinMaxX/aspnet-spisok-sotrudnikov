﻿using System;
using System.Data.Entity.Migrations;
using MvcApplication;

namespace MvcApplication1.Migrations
{
    internal class Configuration : DbMigrationsConfiguration<EfDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EfDbContext context)
        {
            InitializeDatabase.Initialize(context);
        }

        protected void FixEfProviderServicesProblem()
        {
            // The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            // for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            // Make sure the provider assembly is available to the running application. 
            // See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.
            if (typeof(System.Data.Entity.SqlServer.SqlProviderServices) == null)
            {
                throw new Exception("This code is used to ensure that the compiler will include assembly");
            }
        }
    }
}
