﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Interfaces;
using MvcApplication.Models;
using MvcApplication.Properties;
using MvcApplication.Services;

namespace MvcApplication.Controllers
{
    public class AccountController : Controller
    {
        private static readonly int DefaultMinPasswordLength = Settings.Default.MinRequiredPasswordLength;
        protected int MinPasswordLength;
        protected readonly IFormsAuthenticationService FormsService;
        private readonly IAccountService _accounts;

        public AccountController(IFormsAuthenticationService formsService, IAccountService accounts)
        {
            MinPasswordLength = DefaultMinPasswordLength;
            FormsService = formsService;
            _accounts = accounts;
        }

        [HttpGet]
        public ActionResult LogOn(string returnUrl)
        {
            if (User.IsNotGuest())
            {
                return View("AccessDenied");
            }
            return View("LogOn");
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string isNewUser, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (_accounts.ValidateUser(model.UserName, model.Password))
                {
                    var guest = User.GetAccount();
                    if (guest != null && guest.LastCartCount > 0)
                    {
                        _accounts.MoveCart(guest.AccountId, model.UserName);
                    }

                    FormsService.SignIn(model.UserName, model.RememberMe);
                    return Url.IsLocalUrl(returnUrl)
                        ? (ActionResult) Redirect(returnUrl)
                        : RedirectToAction("Index", "Default");
                }
                else if (isNewUser == null)
                {
                    ModelState.AddModelError("", "Имя пользователя или пароль указаны неверно.");
                }
                else
                {
                    if (_accounts.Get(model.UserName) != null)
                    {
                        var err = "Такое имя уже занято. Введите другое имя или Ваш адрес эл.почты.";
                        ModelState.AddModelError("UserName", err);
                    }
                    else
                    {
                        var account = new AccountDataContract
                        {
                            Login = model.UserName,
                            Password = model.Password,
                            Roles = "",
                        };
                        _accounts.AddOrUpdate(account);

                        FormsService.SignIn(model.UserName, model.RememberMe);
                        return RedirectToAction("Register", "Home");
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            FormsService.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewBag.PasswordLength = MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                // if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                ModelState.AddModelError("", "Неправильный текущий пароль или недопустимый новый пароль.");
            }
            ViewBag.PasswordLength = MinPasswordLength;
            return View(model);
        }

        [Authorize]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        // [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id, string login)
        {
            if (login == User.Identity.Name) return View("DeleteSelfDenied");
            if (!User.IsInRole("Admin")) return View("AccessDenied");

            _accounts.Delete(login);
            if (login == User.Identity.Name) return LogOff();
            return RedirectToAction("Edit", "Home", new {Id = id});
        }
    }
}
