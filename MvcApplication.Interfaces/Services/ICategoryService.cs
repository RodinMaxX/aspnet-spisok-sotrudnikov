using System.Collections.Generic;
using System.ServiceModel;
using MvcApplication.DataContract;
using MvcApplication.Entities;

namespace MvcApplication.Services
{
    [ServiceContract]
    public interface ICategoryService
    {
        [OperationContract]
        ICollection<KeyValuePair<CategoryDataContract, int>> WithProductCount();

        [OperationContract]
        bool InUsage(int id);

        [OperationContract]
        List<CategoryDataContract> GetAll();

        [OperationContract]
        CategoryDataContract Get(int id);

        [OperationContract]
        void Delete(int id);

        [OperationContract]
        int AddOrUpdate(CategoryDataContract dataContact);
    }
}