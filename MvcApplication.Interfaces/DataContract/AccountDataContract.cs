using System.Runtime.Serialization;
using MvcApplication.Entities;

namespace MvcApplication.DataContract
{
    [DataContract]
    public sealed class AccountDataContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Roles { get; set; }

        [DataMember]
        public int? EmployeeId { get; set; }

        [DataMember]
        public int LastCartCount { get; set; }

        public AccountDataContract()
        {
        }

        public AccountDataContract(Account account, string roles)
        {
            Id = account.Id;
            Login = account.Login;
            Password = account.Password;
            Roles = roles;
            EmployeeId = account.EmployeeId;
            
            if (account.LastCart != null && account.LastCart.CartItems != null)
            {
                LastCartCount = account.LastCart.CartItems.Count;
            }
        }
    }
}