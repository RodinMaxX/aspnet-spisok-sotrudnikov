﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using MvcApplication.DataContract;
using MvcApplication.Interfaces;

namespace MvcApplication.Entities
{
    public class Group : Entity
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50, ErrorMessage = "Наименование товара не должно быть длиннее 50 символов!")]
        [DisplayName("Название группы")]
        public string Name { get; set; }

        [DisplayName("Роли (права)")]
        public string Roles { get; set; }

        [DisplayName("Учётные записи")]
        public virtual ICollection<Account> Accounts { get; set; }

        public Group()
        {
        }

        public Group(GroupDataContract item)
        {
            Id = item.Id;
            Name = item.Name;
            Roles = item.Roles;
        }

    }
}
