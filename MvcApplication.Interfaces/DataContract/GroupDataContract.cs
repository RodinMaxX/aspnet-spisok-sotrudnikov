﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using MvcApplication.Entities;

namespace MvcApplication.DataContract
{
    [DataContract]
    public sealed class GroupDataContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Roles { get; set; }


        public GroupDataContract()
        {
        }

        public GroupDataContract(Group item)
        {
            Id = item.Id;
            Name = item.Name;
            Roles = item.Roles;
        }
    }
}
