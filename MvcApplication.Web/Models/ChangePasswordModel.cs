using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcApplication.Models
{
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "������� ������")]
        public string OldPassword { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [Display(Name = "����� ������")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "������������� ������")]
        [Compare("NewPassword", ErrorMessage = "����� ������ � ��� ������������� �� ���������.")]
        public string ConfirmPassword { get; set; }
    }
}