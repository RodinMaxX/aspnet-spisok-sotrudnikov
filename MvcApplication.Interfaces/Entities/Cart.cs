﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using MvcApplication.Interfaces;

namespace MvcApplication.Entities
{
    public class Cart : Entity
    {
        [Index]
        public int AccountId { get; set; }

        public virtual ICollection<CartItem> CartItems { get; set; }

        public bool IsSold { get; set; }
    }
}
