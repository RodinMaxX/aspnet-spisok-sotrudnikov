namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Group1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Roles = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GroupAccounts",
                c => new
                    {
                        Group_Id = c.Int(nullable: false),
                        Account_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Group_Id, t.Account_Id })
                .ForeignKey("dbo.Groups", t => t.Group_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.Account_Id, cascadeDelete: true)
                .Index(t => t.Group_Id)
                .Index(t => t.Account_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupAccounts", "Account_Id", "dbo.Users");
            DropForeignKey("dbo.GroupAccounts", "Group_Id", "dbo.Groups");
            DropIndex("dbo.GroupAccounts", new[] { "Account_Id" });
            DropIndex("dbo.GroupAccounts", new[] { "Group_Id" });
            DropTable("dbo.GroupAccounts");
            DropTable("dbo.Groups");
        }
    }
}
