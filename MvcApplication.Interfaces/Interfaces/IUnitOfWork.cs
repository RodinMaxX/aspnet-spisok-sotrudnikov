﻿using System;
using MvcApplication.Entities;

namespace MvcApplication.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<T> Table<T>() where T : Entity;
        void SaveChanges();
    }
}
