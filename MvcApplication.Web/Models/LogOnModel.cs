﻿using System.ComponentModel.DataAnnotations;

namespace MvcApplication.Models
{
    public class LogOnModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Логин является обязательным для заполнения")]
        [StringLength(50, ErrorMessage = "Имя учётной записи не должно быть длиннее 50 символов!")]
        [Display(Name = "Имя пользователя или эл.почта")]
        public string UserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Пароль является обязательным для заполнения")]
        [StringLength(4000, ErrorMessage = "Пароль не должен быть длиннее 4000 символов!")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }
}