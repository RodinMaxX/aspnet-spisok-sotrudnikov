﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Interfaces;

namespace MvcApplication.Services
{
    public class AccountService : ServiceBase<Account, AccountDataContract>, IAccountService
    {
        public AccountService(IUnitOfWork context) 
            : base(context)
        {
        }

        public AccountService()
            : this(new UnitOfWork())
        {
        }

        public bool ValidateUser(string login, string password)
        {
            var account = Get(login);
            return (account != null) && (account.Password == password);
        }

        public AccountDataContract Get(string login)
        { 
            var account = Table.All.FirstOrDefault(x => x.Login == login);
            return (account == null) ? null : new AccountDataContract(account, account.GetRoles());
        }

        public void MoveCart(int? guestId, string login)
        {
            var guest = (guestId.HasValue) ? Table.Get(guestId.Value) : null;
            if (guest != null && !guest.IsCartEmpty)
            {
                var user = Table.All.FirstOrDefault(x => x.Login == login);
                if (user != null && user.IsCartEmpty)
                {
                    user.LastCartId = guest.LastCartId;
                    guest.LastCart = null;
                    Context.SaveChanges();
                }
            }
        }

        public KeyValuePair<AccountDataContract, string> GetWithFullName(string login)
        { 
            var account = Table.All.FirstOrDefault(x => x.Login == login);
            if (account == null) return new KeyValuePair<AccountDataContract, string>();
            
            if (!account.EmployeeId.HasValue)
            {
                return new KeyValuePair<AccountDataContract, string>(ToCotract(account), null);
            }

            var user = new EmployeeDataContract(Context.Table<Employee>().Get(account.EmployeeId.Value));
            return new KeyValuePair<AccountDataContract, string>(ToCotract(account), user.FullName);
        } 
 
        public void Delete(string login)
        {
            var old = Get(login);
            Table.Delete(old.Id);
            Context.SaveChanges();
        }

        public override List<AccountDataContract> GetAll()
        {
            return base.GetAll().Where(x => !x.Login.StartsWith("guest-")).ToList();
        }

        public List<AccountDataContract> Filter(int employeeId)
        {
            return Table.All.Where(x => x.EmployeeId == employeeId).ToList().ConvertAll(ToCotract);
        } 
 
        protected override AccountDataContract ToCotract(Account item)
        {
            return new AccountDataContract(item, item.GetRoles());
        } 
 
        protected override Account FromCotract(AccountDataContract item)
        { 
            return new Account(item);
        }
    }
}
