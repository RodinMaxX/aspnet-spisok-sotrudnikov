using System.Linq;

namespace MvcApplication.Interfaces
{
    public interface IRepository<T>
    {
        IQueryable<T> All { get; }
        T Get(int id);
        void Delete(int id);
        void Add(T item);
        void Update(T item);
    }
}
