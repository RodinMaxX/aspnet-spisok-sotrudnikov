﻿using System.Runtime.Serialization;
using MvcApplication.Entities;

namespace MvcApplication.DataContract
{
    [DataContract]
    public sealed class ProductDataContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? CategoryId { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        public ProductDataContract()
        {
        }

        public ProductDataContract(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Description = product.Description;
            CategoryId = product.CategoryId;
            Price = product.Price;
        }
    }
}
