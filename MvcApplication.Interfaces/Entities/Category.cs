﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MvcApplication.DataContract;
using MvcApplication.Interfaces;

namespace MvcApplication.Entities
{
    public class Category : Entity
    {
        [Required(AllowEmptyStrings = false), Index]
        [StringLength(50, ErrorMessage = "Название категории не должно быть длиннее 50 символов!")]
        [DisplayName("Название категории")]
        public string Name { get; set; }
            
        public virtual ICollection<Product> Products { get; set; }

        public Category()
        {
        }

        public Category(CategoryDataContract category)
        {
            Id = category.Id;
            Name = category.Name;
        }
    }
}