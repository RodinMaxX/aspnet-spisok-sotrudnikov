namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Password = c.String(),
                        Roles = c.String(),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        SurName = c.String(nullable: false, maxLength: 50),
                        Patronymic = c.String(maxLength: 50),
                        BirthDate = c.DateTime(),
                        Position = c.String(maxLength: 50),
                        BeginWorkDate = c.DateTime(),
                        Login = c.String(maxLength: 50),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Accounts", new[] { "EmployeeId" });
            DropTable("dbo.Employees");
            DropTable("dbo.Accounts");
        }
    }
}
