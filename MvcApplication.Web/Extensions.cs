﻿using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using MvcApplication.Models;

public static partial class Extensions
{
    public static string IsSelected(this HtmlHelper html, string controllers = "", string actions = "", string cssClass = "active")
    {
        var context = html.ViewContext;
        if (context.Controller.ControllerContext.IsChildAction)
        {
            context = html.ViewContext.ParentActionViewContext;
        }

        var values = context.RouteData.Values;
        var currentAction = values["action"].ToString();
        var currentController = values["controller"].ToString();
        if (string.IsNullOrEmpty(actions)) actions = currentAction;
        if (string.IsNullOrEmpty(controllers)) controllers = currentController;

        var actionArray = actions.Trim().Split(',');
        var controllerArray = controllers.Trim().Split(',');
        var isActive = actionArray.Contains(currentAction) && controllerArray.Contains(currentController);

        return isActive ? cssClass : "";
    }

    public static bool IsNotGuest(this IPrincipal user)
    {
        if (!user.Identity.IsAuthenticated) return false;
        var account = user.Identity as AccountIdentity;
        return (account != null) && (account.FullName != null);
    }

    public static bool HasLastCart(this IPrincipal user)
    {
        if (!user.Identity.IsAuthenticated) return false;
        var account = user.Identity as AccountIdentity;
        return (account != null) && (account.LastCartCount > 0);
    }

    public static AccountIdentity GetAccount(this IPrincipal user)
    {
        if (!user.Identity.IsAuthenticated) return null;
        return user.Identity as AccountIdentity;
    }
}
