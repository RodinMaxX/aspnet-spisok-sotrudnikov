﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Interfaces;
using MvcApplication.Models;

namespace MvcApplication.Services
{
    public class ProductService : ServiceBase<Product, ProductDataContract>, IProductService
    {
        public ProductService(IUnitOfWork context)
            : base(context)
        {
        }

        public ProductService()
            : this(new UnitOfWork())
        {
        }

        public List<ProductDataContract> Filter(FilterModel f)
        {
            var query = Table.All;
            if (f != null)
            {
                if (f.CategoryId.HasValue) query = query.Where(x => x.CategoryId == f.CategoryId.Value);
                if (f.MinPrice.HasValue) query = query.Where(x => x.Price >= f.MinPrice.Value);
                if (f.MaxPrice.HasValue) query = query.Where(x => x.Price <= f.MaxPrice.Value);
                if (!String.IsNullOrWhiteSpace(f.Text))
                {
                    foreach (var word in f.Text.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (f.OnlyName == true) query = query.Where(x => x.Name.Contains(word));
                        else query = query.Where(x => x.Name.Contains(word) || x.Description.Contains(word));
                    }
                }
            }
            return query.ToList().ConvertAll(x => new ProductDataContract(x));
        }

        protected override ProductDataContract ToCotract(Product item)
        {
            return new ProductDataContract(item);
        }

        protected override Product FromCotract(ProductDataContract item)
        {
            return new Product(item);
        }
    }
}
