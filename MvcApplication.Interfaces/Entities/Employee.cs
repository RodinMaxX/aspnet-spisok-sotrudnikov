﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MvcApplication.DataContract;
using MvcApplication.Interfaces;

namespace MvcApplication.Entities
{
    public class Employee : Entity
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50, ErrorMessage = "Имя не должно быть длиннее 50 символов!")]
        [DisplayName("Имя")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(50, ErrorMessage = "Фамилия не должна быть длиннее 50 символов!")]
        [DisplayName("Фамилия")]
        public string SurName { get; set; }

        [DisplayName("Отчество")]
        [StringLength(50, ErrorMessage = "Отчество не должна быть длиннее 50 символов!")]
        public string Patronymic { get; set; }

        [DataType(DataType.EmailAddress)]
        [DisplayName ( "Адрес электронной почты")]
        public string Email { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true)]
        [DisplayName("Дата рождения")]
        public DateTime? BirthDate { get; set; }

        [DisplayName("Должность")]
        [StringLength(50, ErrorMessage = "Название должности не должно быть длиннее 50 символов!")]
        public string Position { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true)]
        [DisplayName("Дата начала работы")]
        public DateTime? BeginWorkDate { get; set; }

        [DisplayName("Учётные записи")]
        public virtual ICollection<Account> Accounts { get; set; }

        [NotMapped]
        [StringLength(50, ErrorMessage = "Длина логина не должно быть длиннее 50 символов!")]
        public string Login { get; set; }

        [NotMapped]
        public bool IsAdmin { get; set; }
        public Employee()
        {
        }

        public Employee(EmployeeDataContract employee)
        {
            Id = employee.Id;
            SurName = employee.SurName;
            FirstName = employee.FirstName;
            Patronymic = employee.Patronymic;
            BirthDate = employee.BirthDate;
            Position = employee.Position;
            BeginWorkDate = employee.BeginWorkDate;
        }
    }
}