using System.Collections.Generic;
using System.Linq;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Interfaces;

namespace MvcApplication.Services
{
    public class CategoryService : ServiceBase<Category, CategoryDataContract>, ICategoryService
    {
        public CategoryService(IUnitOfWork context)
            : base(context)
        {
        }

        public CategoryService()
            : this(new UnitOfWork())
        {            
        }

        public ICollection<KeyValuePair<CategoryDataContract, int>> WithProductCount()
        {
            var query = Table.All.Select(category => new {category, category.Products.Count});
            return query.ToDictionary(x => new CategoryDataContract(x.category), x => x.Count);
        }

        public bool InUsage(int id)
        {
            return Table.All.Where(x => id == x.Id).Select(x => x.Products.Count).FirstOrDefault() > 0;
        }

        protected override CategoryDataContract ToCotract(Category item)
        {
            return new CategoryDataContract(item);
        }

        protected override Category FromCotract(CategoryDataContract item)
        {
            return new Category(item);
        }
    }
}
