﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MvcApplication.DataContract;
using MvcApplication.Interfaces;

namespace MvcApplication.Entities
{
    public class Product : Entity
    {
        [Required(AllowEmptyStrings = false)]
        [Index]
        [StringLength(50, ErrorMessage = "Наименование товара не должно быть длиннее 50 символов!")]
        [DisplayName("Наименование")]
        public string Name { get; set; }

        [DisplayName("Описание")]
        public string Description { get; set; }

        [Index]
        [DisplayName("Категория")]
        public int? CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [Index]
        [DisplayName("Цена")]
        public decimal Price { get; set; }

        public Product()
        {
        }

        public Product(ProductDataContract product)
        {
            Id = product.Id;
            Name = product.Name;
            Description = product.Description;
            CategoryId = product.CategoryId;
            Price = product.Price;
        }
    }
}
