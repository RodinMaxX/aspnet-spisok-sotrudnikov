﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcApplication.DataContract;
using MvcApplication.Entities;

namespace MvcApplication.Models
{
    public class EditGroupModel
    {
        public Group Group { get; set; }
        public IList<AccountDataContract> AllAccounts { get; set; }

        public bool InGroup(int accountId)
        {
            return Group.Accounts.Any(x => x.Id == accountId);
        }
    }
}