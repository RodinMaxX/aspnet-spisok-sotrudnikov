﻿using System.Runtime.Serialization;
using MvcApplication.Entities;

namespace MvcApplication.DataContract
{
    [DataContract]
    public sealed class CartItemDataContract
    {
        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public int Count { get; set; }

        public CartItemDataContract()
        {
        }

        public CartItemDataContract(CartItem item)
        {
            ProductId = item.ProductId;
            Count = item.Count;
        }
    }
}
