namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cart2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Users", new[] { "EmployeeId" });
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        IsSold = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.CartItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        CartId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carts", t => t.CartId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.CartId);
            
            AddColumn("dbo.Users", "LastCartId", c => c.Int());
            AlterColumn("dbo.Users", "EmployeeId", c => c.Int());
            CreateIndex("dbo.Users", "EmployeeId");
            CreateIndex("dbo.Users", "LastCartId");
            AddForeignKey("dbo.Users", "LastCartId", "dbo.Carts", "Id");
            AddForeignKey("dbo.Users", "EmployeeId", "dbo.Employees", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Users", "LastCartId", "dbo.Carts");
            DropForeignKey("dbo.CartItems", "CartId", "dbo.Carts");
            DropIndex("dbo.CartItems", new[] { "CartId" });
            DropIndex("dbo.CartItems", new[] { "ProductId" });
            DropIndex("dbo.Carts", new[] { "AccountId" });
            DropIndex("dbo.Users", new[] { "LastCartId" });
            DropIndex("dbo.Users", new[] { "EmployeeId" });
            AlterColumn("dbo.Users", "EmployeeId", c => c.Int(nullable: false));
            DropColumn("dbo.Users", "LastCartId");
            DropTable("dbo.CartItems");
            DropTable("dbo.Carts");
            CreateIndex("dbo.Users", "EmployeeId");
            AddForeignKey("dbo.Users", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
