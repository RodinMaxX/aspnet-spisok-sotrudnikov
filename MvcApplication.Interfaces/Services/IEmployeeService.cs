using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using MvcApplication.DataContract;
using MvcApplication.Entities;

namespace MvcApplication.Services
{
    [ServiceContract]
    public interface IEmployeeService
    {
        [OperationContract]
        void ExportCsv(string filepath);

        [OperationContract]
        List<EmployeeDataContract> GetAll();

        [OperationContract]
        EmployeeDataContract Get(int id);

        [OperationContract]
        void Delete(int id);

        [OperationContract]
        int AddOrUpdate(EmployeeDataContract product);
    }
}