﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Models;
using MvcApplication.Services;

namespace MvcApplication.Controllers
{
    public class DefaultController : Controller
    {
        private readonly IProductService _products;
        private readonly ICategoryService _categories;
        private readonly ICartService _cart;
        private readonly IFormsAuthenticationService _authentication;

        public DefaultController(IProductService products, ICategoryService categories, ICartService cart, IFormsAuthenticationService authentication)
        {
            _products = products;
            _categories = categories;
            _cart = cart;
            _authentication = authentication;
        }

        public ActionResult Index(ProductsAndCategories model, FilterModel filter, string isCart)
        {
            ICollection<CartItem> cartItems = (model != null) ? model.CartItems : null;
            filter = filter ?? ((model != null) ? model.Filter : null);
            model = AddCategories(_products.Filter(filter), filter);
            var user = User.GetAccount();
            var accountId = (user == null) ? null : user.AccountId;
            if (cartItems != null)
            {
                var oldCart = (accountId.HasValue)
                    ? new HashSet<int>(_cart.Filter(accountId.Value).Select(x => x.ProductId))
                    : new HashSet<int>();
                foreach (var item in cartItems)
                {
                        if (item.Count > 1 && !oldCart.Contains(item.ProductId)) item.IsSelected = "on";
                }
                model.CartItems = cartItems;

                var cart = cartItems.Where(x => x.IsSelected != null).Select(x => new CartItemDataContract(x));
                string newUserName = _cart.AddOrUpdate(accountId, cart.ToList());
                _authentication.SignIn(newUserName, true);
            }
            else if (accountId.HasValue)
            {
                cartItems = _cart.Filter(accountId.Value).Select(x => new CartItem(x) { IsSelected = "on" }).ToList();
                model.CartItems = cartItems;
            }
            if (isCart != null) return RedirectToAction("Cart", model.Filter);
            return View(model);
        }

        [HttpGet]
        public ActionResult Cart(FilterModel filter)
        {
            var user = User.GetAccount();
            if (user == null || !user.AccountId.HasValue) return View(AddCategories(new ProductDataContract[0], filter));
            var model = AddCategories(_cart.GetProducts(user.AccountId.Value), filter);
            model.CartItems = _cart.Filter(user.AccountId.Value).Select(x => new CartItem(x) { IsSelected = "on" }).ToList();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Cart(ProductsAndCategories model, FilterModel filter, string isBuy)
        {
            filter = filter ?? ((model != null) ? model.Filter : null);
            var user = User.GetAccount();
            var cart = (from item in model.CartItems
                        where item.IsSelected != null
                        select new CartItemDataContract(item)
                       ).ToList();
            _cart.AddOrUpdate(user.AccountId.Value, cart);

            if (isBuy != null && cart.Any())
            {
                if (User.IsNotGuest())
                {
                    _cart.FinishBuy(User.GetAccount().AccountId);
                    return RedirectToAction("Buy");
                }
                return RedirectToAction("Register", "Home");
            }
            var newModel = AddCategories(_cart.GetProducts(user.AccountId.Value), filter);
            newModel.CartItems = model.CartItems;
            return View(newModel);
        }

        [Authorize(Roles = "Admin,Seller")]
        [HttpPost]
        public ActionResult Edit(ProductsAndCategories model, string isDeleted)
        {
            if (isDeleted != null)
            {
                _products.Delete(model.Product.Id);
            }
            else
            {
                if (!ModelState.IsValid) return View(AddCategories(model.Products, model.Filter));
                _products.AddOrUpdate(new ProductDataContract(model.Product));
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var product = (id == null) ? new ProductDataContract() : _products.Get(id.Value);
            if (product != null) return View(AddCategories(new[] { product}));
            return new HttpNotFoundResult("Нет продукта с id = " + id);
        }

        private ProductsAndCategories AddCategories(IList<ProductDataContract> products, FilterModel filter = null)
        {
            return new ProductsAndCategories(products, _categories.GetAll()) { Filter = filter};
        }

        public ActionResult Buy()
        {
            return View();
        }
    }
}
