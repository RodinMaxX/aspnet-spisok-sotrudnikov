﻿using System;
using System.Linq;
using MvcApplication;
using MvcApplication.Entities;

namespace MvcApplication1.Migrations
{
    internal static class InitializeDatabase
    {
        public static void Initialize(EfDbContext context)
        {
            var admin = context.Accounts.FirstOrDefault(x => x.Login == "admin");
            admin = admin ?? AccountsArray.First(x => x.Login == "admin");
            if (admin.Groups == null || !admin.Groups.Any()) admin.Groups = AdminGroup;
            
            if (!context.Employees.Any()) context.Employees.AddRange(EmployeesArray);
            if (!context.Accounts.Any()) context.Accounts.AddRange(AccountsArray);
        }

        private static readonly Employee[] EmployeesArray = 
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Иван",
                    SurName = "Иванов",
                    Patronymic = "Иванович",
                    BirthDate = new DateTime(1955,05,21),
                    Position = "Инженер",
                    BeginWorkDate = DateTime.Parse("03.07.2000")
                },
                new Employee
                {
                    Id = 2,
                    FirstName = "Владислав",
                    SurName = "Петров",
                    Patronymic = "Иванович",
                    BirthDate = new DateTime(1952,05,22),
                    Position = "Директор",
                    BeginWorkDate = DateTime.Parse("03.06.2000")
                },
                new Employee
                {
                    Id = 3,
                    FirstName = "Петр",
                    SurName = "Петров",
                    Patronymic = "Петрович",
                    BirthDate = DateTime.Parse("24.08.1995"),
                    Position = "Финансовый директор",
                    BeginWorkDate = DateTime.Parse("03.08.2001")
                },

                new Employee
                {
                    Id = 4,
                    FirstName = "Сидор",
                    SurName = "Сидоров",
                    Patronymic = "Иванович",
                    BirthDate = DateTime.Parse("24.08.1987"),
                    Position = "Коммерческий директор",
                    BeginWorkDate = DateTime.Parse("03.09.2002")
                },
            };

        private static readonly Account[] AccountsArray = new[]
        {
            new Account { Login = "admin", Password = "12345", EmployeeId = 2,},
            new Account { Login = "user", Password = "12345", EmployeeId = 3,},
        };

        private static readonly Group[] AdminGroup = new[]
        {
            new Group {Name = "Администраторы", Roles = "Admin"},
        };
    }
}