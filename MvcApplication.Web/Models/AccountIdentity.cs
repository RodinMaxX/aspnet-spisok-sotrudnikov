﻿using System.Security.Principal;
using System.Web;
using System.Web.Security;
using MvcApplication.Services;

namespace MvcApplication.Models
{
    public class AccountIdentity : FormsIdentity
    {
        public string[] Roles { get; private set; }
        public string FullName { get; private set; }
        public int? AccountId { get; private set; }
        public int? EmployeeId { get; private set; }
        public int LastCartCount { get; set; }

        private AccountIdentity(IAccountService accounts, FormsAuthenticationTicket ticket)
            : base(ticket)
        {
            var user = accounts.GetWithFullName(ticket.Name);
            if (user.Key != null)
            {
                Roles = user.Key.Roles.Split(',');
                FullName = user.Value;
                AccountId = user.Key.Id;
                EmployeeId = user.Key.EmployeeId;
                LastCartCount = user.Key.LastCartCount;
            }
        }

        public static void TrySetIdentity(IAccountService accounts, HttpContext current)
        {
            var identity = (current.User == null) ? null : current.User.Identity as FormsIdentity;
            if (identity != null && identity.IsAuthenticated)
            {
                var newIdentity = identity as AccountIdentity ?? new AccountIdentity(accounts, identity.Ticket);
                current.User = new AccountPrincipal(newIdentity);
            }
        }

        private class AccountPrincipal : GenericPrincipal
        {
            public AccountPrincipal(AccountIdentity identity)
                : base(identity, identity.Roles)
            {
            }
        }
    }
}