﻿using System.Collections.Generic;
using System.Linq;
using MvcApplication.DataContract;
using MvcApplication.Interfaces;

namespace MvcApplication.Services
{
    public abstract class ServiceBase<TDatabase, TContract>
        where TDatabase : Entity
    {
        protected readonly IUnitOfWork Context;
        protected readonly IRepository<TDatabase> Table;

        protected ServiceBase(IUnitOfWork context)
        {
            Context = context;
            Table = context.Table<TDatabase>();
        }

        protected abstract TContract ToCotract(TDatabase item);
        protected abstract TDatabase FromCotract(TContract item);

        public virtual List<TContract> GetAll()
        {
            return Table.All.ToList().ConvertAll(ToCotract);
        }

        public TContract Get(int id)
        {
            return ToCotract(Table.Get(id));
        }

        public void Delete(int id)
        {
            Table.Delete(id);
            Context.SaveChanges();
        }

        public int AddOrUpdate(TContract dataContact)
        {
            var item = FromCotract(dataContact);
            if (item.Id == 0) Table.Add(item);
            else Table.Update(item);
            Context.SaveChanges();
            return item.Id;
        }
    }
}