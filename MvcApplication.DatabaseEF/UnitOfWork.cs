﻿using MvcApplication.Entities;
using MvcApplication.Interfaces;

namespace MvcApplication
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EfDbContext _context;

        public UnitOfWork()
        {
            _context = new EfDbContext();
        }

        public IRepository<T> Table<T>()
            where T : Entity
        {
            return new Repository<T>(_context);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}