﻿using System.Collections.Generic;
using System.ServiceModel;
using MvcApplication.DataContract;
using MvcApplication.Models;

namespace MvcApplication.Services
{
    [ServiceContract]
    public interface IProductService
    {
        [OperationContract]
        List<ProductDataContract> Filter(FilterModel filter);

        [OperationContract]
        ProductDataContract Get(int id);

        [OperationContract]
        void Delete(int id);

        [OperationContract]
        int AddOrUpdate(ProductDataContract product);
    }
}