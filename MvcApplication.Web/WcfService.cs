﻿using System;
using System.Linq;
using System.ServiceModel;

namespace MvcApplication
{
    public static class WcfService
    {
        public static T GetNew<T>()
            where T : class // allowed interface type only
        {
            var name = typeof(T).Name;
            if (!typeof(T).IsInterface || name.FirstOrDefault() == 'I') name = name.Substring(1);
            else throw new ArgumentException("Allowed interface type only with first 'I' letter in name");

            var address = new EndpointAddress("http://localhost:18181/" + name + ".svc");
            return new ChannelFactory<T>(new BasicHttpBinding(), address).CreateChannel();
        }
    }
}