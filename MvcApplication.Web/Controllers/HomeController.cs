﻿using System;
using System.Linq;
using System.Web.Mvc;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Properties;
using MvcApplication.Services;
using MvcApplication.Models;

namespace MvcApplication.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        protected readonly IEmployeeService _employees;
        private readonly IAccountService _accounts;

        public HomeController(IEmployeeService employeeService, IAccountService accounts)
        {
            _employees = employeeService;
            _accounts = accounts;
        }

        public ActionResult Index()
        {
            return View(_employees.GetAll());
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Employee model, string isDeleted)
        {
            if (isDeleted != null)
            {
                var user = User.Identity as AccountIdentity;
                if (user != null && user.EmployeeId == model.Id) return View("../Account/DeleteSelfDenied");
                _employees.Delete(model.Id);
            }
            else 
            {
                if (!UpdateEmployee(model)) return View(AddAccounts(model));
            }
            return RedirectToAction("Index");
        }

        private bool UpdateEmployee(Employee model)
        {
                CheckDate(model.BirthDate,"BirthDate");
                CheckDate(model.BeginWorkDate, "BeginWorkDate");
                if (!ModelState.IsValid) return false;
                model.Id = _employees.AddOrUpdate(new EmployeeDataContract(model));

                if (!string.IsNullOrWhiteSpace(model.Login))
                {
                    var account = new AccountDataContract
                    {
                        EmployeeId = model.Id,
                        Login = model.Login,
                        Password = "12345",
                        Roles = model.IsAdmin ? "Admin" : "User",
                    };
                    _accounts.AddOrUpdate(account);
                }
                return true;
        }

        [HttpPost]
        public ActionResult Register(Employee model)
        {
            var user = User.Identity as AccountIdentity;
            if (user == null) return RedirectToAction("LogOn", "Account");
            if (user.EmployeeId != model.Id) return View("../Account/AccessDenied");

            if (ModelState.IsValid && UpdateEmployee(model))
            {
                return RedirectToAction("Index", "Default");
            }
            return View(AddAccounts(model));
        }

        [HttpGet]
        public ActionResult Register()
        {
            var user = _employees.Get(User.GetAccount().EmployeeId.Value);
            return View(AddAccounts(new Employee(user)));
        }

        private void CheckDate(DateTime? date, string fieldName)
        {
            if (date > DateTime.Now)
            {
                ModelState.AddModelError(fieldName, "Принимается только уже наступившая дата!!!");
            }
            if (date.HasValue && date.Value.Year < 1870)
            {
                ModelState.AddModelError(fieldName, "Указываемая Вами дата должна быть больше 1870!!!");
            }
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var user = (id == null)? new EmployeeDataContract() : _employees.Get(id.Value);
            if (user != null) return View(AddAccounts(new Employee(user)));
            return new HttpNotFoundResult("Нет пользователя с id = " + id);
        }

        [Authorize(Roles="Admin")]
        public ActionResult SaveFile()
        {
            _employees.ExportCsv(Settings.Default.UsersCsvPath);
            return RedirectToAction("Index");
        }

        private Employee AddAccounts(Employee employee)
        {
            var accounts = _accounts.Filter(employee.Id);
            employee.Accounts = accounts.ConvertAll(x => new Account(x));
            return employee;
        }
    }
}
