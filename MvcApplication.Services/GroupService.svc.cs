using System.Collections.Generic;
using System.Linq;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Interfaces;

namespace MvcApplication.Services
{
    public class GroupService : ServiceBase<Group, GroupDataContract>, IGroupService
    {
        public GroupService(IUnitOfWork context)
            : base(context)
        {
        }

        public GroupService()
            : this(new UnitOfWork())
        {            
        }

        public ICollection<KeyValuePair<GroupDataContract, int>> WithAccountCount()
        {
            var query = Table.All.Select(group => new { group, group.Accounts.Count });
            return query.ToDictionary(x => new GroupDataContract(x.group), x => x.Count);
        }

        public bool InUsage(int id)
        {
            return Table.All.Where(x => id == x.Id).Select(x => x.Accounts.Count).FirstOrDefault() > 0;
        }

        public int AddOrUpdate(GroupDataContract groupContract, List<int> selected)
        {
            var groupId = base.AddOrUpdate(groupContract);
            var group = Table.Get(groupId);
            if (group.Accounts == null) group.Accounts = new List<Account>();
            foreach (var account in group.Accounts.ToList())
            {
                if (selected.Contains(account.Id)) selected.Remove(account.Id);
                else group.Accounts.Remove(account);
            }
            foreach (var accountId in selected)
            {
                group.Accounts.Add(Context.Table<Account>().Get(accountId));
            }
            Context.SaveChanges();
            return groupId;
        }

        public List<AccountDataContract> GetAccounts(int? id)
        {
            var accounts = Context.Table<Account>().All.Where(x => x.Groups.Any(y => y.Id == id));
            return accounts.ToList().ConvertAll(x => new AccountDataContract(x, x.GetRoles()));
        }

        protected override GroupDataContract ToCotract(Group item)
        {
            return new GroupDataContract(item);
        }

        protected override Group FromCotract(GroupDataContract item)
        {
            return new Group(item);
        }
    }
}
