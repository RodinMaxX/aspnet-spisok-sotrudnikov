﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MvcApplication.Controllers;
using MvcApplication.Interfaces;
using MvcApplication.Models;
using MvcApplication.Services;
using Ninject;

namespace MvcApplication
{
    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Имя маршрута
                "{controller}/{action}/{id}", // URL-адрес с параметрами
                new {controller = "Default", action = "Index", id = UrlParameter.Optional} // Параметры по умолчанию
            );
        }

        protected void Application_Start()
        {
            var ninjectController = new NinjectControllerFactory {Kernel = CreateKernel()};
            ControllerBuilder.Current.SetControllerFactory(ninjectController);
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            var accounts = WcfService.GetNew<IAccountService>();
            AccountIdentity.TrySetIdentity(accounts, HttpContext.Current);
        }

        protected IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Bind<IFormsAuthenticationService>().To<FormsAuthenticationService>();
            kernel.Bind<IAccountService>().ToMethod<IAccountService>(x => WcfService.GetNew<IAccountService>());
            kernel.Bind<ICartService>().ToMethod<ICartService>(x => WcfService.GetNew<ICartService>());
            kernel.Bind<ICategoryService>().ToMethod<ICategoryService>(x => WcfService.GetNew<ICategoryService>());
            kernel.Bind<IGroupService>().ToMethod<IGroupService>(x => WcfService.GetNew<IGroupService>());
            kernel.Bind<IEmployeeService>().ToMethod<IEmployeeService>(x => WcfService.GetNew<IEmployeeService>());
            kernel.Bind<IProductService>().ToMethod<IProductService>(x => WcfService.GetNew<IProductService>());
            return kernel;
        }

        private class NinjectControllerFactory : DefaultControllerFactory
        {
            public IKernel Kernel;

            protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
            {
                return (controllerType == null) ? null : (IController)Kernel.Get(controllerType);
            }
        }
    }
}
