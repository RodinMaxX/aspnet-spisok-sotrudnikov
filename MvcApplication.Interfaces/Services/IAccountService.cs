using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using MvcApplication.DataContract;
using MvcApplication.Entities;

namespace MvcApplication.Services
{
    [ServiceContract]
    public interface IAccountService
    {
        [OperationContract] 
        bool ValidateUser(string login, string password);

        [OperationContract]
        KeyValuePair<AccountDataContract, string> GetWithFullName(string login);

        [OperationContract] 
        void Delete(string login);

        [OperationContract] 
        int AddOrUpdate(AccountDataContract product);

        [OperationContract] 
        List<AccountDataContract> Filter(int employeeId);

        [OperationContract]
        AccountDataContract Get(string login);

        [OperationContract]
        void MoveCart(int? guestId, string login);

        [OperationContract]
        List<AccountDataContract> GetAll();
    }
}