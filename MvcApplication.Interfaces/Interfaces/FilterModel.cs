﻿using System.Runtime.Serialization;

namespace MvcApplication.Models
{
    [DataContract]
    public class FilterModel
    {
        [DataMember]
        public int? CategoryId { get; set; }

        [DataMember]
        public decimal? MinPrice { get; set; }

        [DataMember]
        public decimal? MaxPrice { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public bool? OnlyName { get; set; }
    }
}