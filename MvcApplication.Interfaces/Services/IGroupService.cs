using System.Collections.Generic;
using System.ServiceModel;
using MvcApplication.DataContract;
using MvcApplication.Entities;

namespace MvcApplication.Services
{
    [ServiceContract]
    public interface IGroupService
    {
        [OperationContract]
        ICollection<KeyValuePair<GroupDataContract, int>> WithAccountCount();

        [OperationContract]
        bool InUsage(int id);

        [OperationContract]
        List<GroupDataContract> GetAll();

        [OperationContract]
        GroupDataContract Get(int id);

        [OperationContract]
        void Delete(int id);

        [OperationContract]
        int AddOrUpdate(GroupDataContract group, List<int> selected);

        [OperationContract]
        List<AccountDataContract> GetAccounts(int? id);
    }
}