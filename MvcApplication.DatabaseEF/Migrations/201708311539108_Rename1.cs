namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rename1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Accounts", newName: "Users");
            DropIndex("dbo.Users", new[] { "EmployeeId" });
            RenameColumn(table: "dbo.Users", name: "Roles", newName: "Role");
            CreateIndex("dbo.Users", "Login", unique: true);
            CreateIndex("dbo.Users", "EmployeeId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "EmployeeId" });
            DropIndex("dbo.Users", new[] { "Login" });
            RenameColumn(table: "dbo.Users", name: "Role", newName: "Roles");
            CreateIndex("dbo.Users", "EmployeeId");
            RenameTable(name: "dbo.Users", newName: "Accounts");
        }
    }
}
