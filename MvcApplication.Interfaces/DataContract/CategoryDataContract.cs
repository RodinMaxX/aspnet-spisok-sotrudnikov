using System.Runtime.Serialization;
using MvcApplication.Entities;

namespace MvcApplication.DataContract
{
    [DataContract]
    public sealed class CategoryDataContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        public CategoryDataContract()
        {
        }

        public CategoryDataContract(Category category)
        {
            Id = category.Id;
            Name = category.Name;
        }
    }
}