﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using MvcApplication.DataContract;
using MvcApplication.Interfaces;

namespace MvcApplication.Entities
{
    [Table("Users")]
    public class Account : Entity
    {
        [Index(IsUnique = true), Required(AllowEmptyStrings = false)]
        [StringLength(50, ErrorMessage = "Имя учётной записи не должно быть длиннее 50 символов!")]
        public string Login { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }

        public virtual ICollection<Group> Groups { get; set; }

        [Index]
        public int? EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }

        public int? LastCartId { get; set; }
        
        [ForeignKey("LastCartId")]
        public virtual Cart LastCart { get; set; }

        [NotMapped]
        public string IsSelected { get; set; }

        [NotMapped]
        public bool IsCartEmpty
        {
            get { return LastCart != null && LastCart.CartItems != null && LastCart.CartItems.Count == 0; }
        }

        public Account()
        {
        }

        public Account(AccountDataContract account)
        {
            Id = account.Id;
            Login = account.Login;
            Password = account.Password;
            EmployeeId = account.EmployeeId;
        }

        public string GetRoles()
        {
            return string.Join(",", Groups.Select(x => x.Roles).Where(x => !string.IsNullOrWhiteSpace(x)));
        }
    }
}