namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Check2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.CartItems", new[] { "ProductId" });
            DropIndex("dbo.CartItems", new[] { "CartId" });
            CreateIndex("dbo.CartItems", new[] { "CartId", "ProductId" }, unique: true, name: "IX_CartAndProduct");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CartItems", "IX_CartAndProduct");
            CreateIndex("dbo.CartItems", "CartId");
            CreateIndex("dbo.CartItems", "ProductId");
        }
    }
}
