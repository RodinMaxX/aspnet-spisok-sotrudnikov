﻿using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Text;
using MvcApplication.Entities;
using MvcApplication1.Migrations;

namespace MvcApplication
{
    internal class EfDbContext : DbContext
    {
        public EfDbContext() 
            : base("DbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EfDbContext, Configuration>());
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var error = new StringBuilder(ex.Message);
                foreach (DbEntityValidationResult validationError in ex.EntityValidationErrors)
                {
                    error.Append(" Object: ");
                    error.Append(validationError.Entry.Entity);
                    error.Append(": ");
                    foreach (DbValidationError err in validationError.ValidationErrors)
                    {
                        error.Append(err.ErrorMessage);
                        error.Append(", ");
                    }
                }
                throw new DbEntityValidationException(error.ToString(), ex);
            }
        }
    }
}