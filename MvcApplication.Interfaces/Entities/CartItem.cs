﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using MvcApplication.DataContract;
using MvcApplication.Interfaces;

namespace MvcApplication.Entities
{
    public class CartItem : Entity 
    {
        [Index("IX_CartAndProduct", 2, IsUnique = true)]
        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        public int Count { get; set; }
        
        [Index("IX_CartAndProduct", 1, IsUnique = true)]
        public int CartId { get; set; }
       
        [NotMapped]
        public string IsSelected { get; set; }

        [ForeignKey("CartId")]
        public virtual Cart Cart { get; set; }

        public CartItem()
        {
        }

        public CartItem(CartItemDataContract item)
        {
            ProductId = item.ProductId;
            Count = item.Count;
        }
    }
}
