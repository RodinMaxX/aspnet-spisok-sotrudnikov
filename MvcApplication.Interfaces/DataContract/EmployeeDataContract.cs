﻿using System;
using System.Runtime.Serialization;
using MvcApplication.Entities;

namespace MvcApplication.DataContract
{
    [DataContract]
    public sealed class EmployeeDataContract
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string SurName { get; set; }

        [DataMember]
        public string Patronymic { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }

        [DataMember]
        public string Position { get; set; }

        [DataMember]
        public DateTime? BeginWorkDate { get; set; }

        public string FullName
        {
            get { return string.Join(" ", SurName, FirstName, Patronymic); }
        }

        // [DisplayName("Возраст")]
        public int? Age
        {
            get
            {
                if (BirthDate == null) return null;
                int age = DateTime.Now.Year - BirthDate.Value.Year;
                return DateTime.Now < BirthDate.Value.AddYears(age) ? --age : age;
            }
        }

        public override string ToString()
        {
            return String.Join(";", Id, SurName, FirstName, Patronymic, BirthDate, Age, Position, BeginWorkDate);
        }

        public static EmployeeDataContract Parse(string str)
        {
            var s = str.Split(';');
            return new EmployeeDataContract
            {
                Id = int.Parse(s[0]),
                SurName = s[1],
                FirstName = s[2],
                Patronymic = s[3],
                BirthDate = DateTime.Parse(s[4]),
                Position = s[6],
                BeginWorkDate = DateTime.Parse(s[7])
            };
        }

        public EmployeeDataContract()
        {
        }

        public EmployeeDataContract(Employee s)
        {
            Id = s.Id;
            SurName = s.SurName;
            FirstName = s.FirstName;
            Patronymic = s.Patronymic;
            BirthDate = s.BirthDate;
            Position = s.Position;
            BeginWorkDate = s.BeginWorkDate;
        }
    }
}