namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Check1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "Login", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Roles", c => c.String(nullable: false));
            DropColumn("dbo.Employees", "Login");
            DropColumn("dbo.Employees", "IsAdmin");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "IsAdmin", c => c.Boolean(nullable: false));
            AddColumn("dbo.Employees", "Login", c => c.String(maxLength: 50));
            AlterColumn("dbo.Accounts", "Roles", c => c.String());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Accounts", "Login", c => c.String());
        }
    }
}
