using System.Web.Mvc;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Services;

namespace MvcApplication.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categories;

        public CategoryController(ICategoryService category)
        {
            _categories = category;
        }

        public ActionResult Index()
        {
            return View(_categories.WithProductCount());
        }
       
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(Category model, string isDeleted)
        {
            if (isDeleted != null)
            {
                if (_categories.InUsage(model.Id)) return View("CategoryInUsage");
                _categories.Delete(model.Id);
            }
            else
            {
                if (!ModelState.IsValid) return View(model);
                _categories.AddOrUpdate(new CategoryDataContract(model));
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var category = (id == null) ? new CategoryDataContract() : _categories.Get(id.Value);
            if (category != null) return View(new Category(category));
            return new HttpNotFoundResult("Нет категории с id = " + id);
        }
    }
}
