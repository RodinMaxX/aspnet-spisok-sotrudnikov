﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Interfaces;

namespace MvcApplication.Services
{
    public class EmployeeService : ServiceBase<Employee, EmployeeDataContract>, IEmployeeService
    {
        public EmployeeService(IUnitOfWork context)
            : base(context)
        {
        }

        public EmployeeService()
            : this(new UnitOfWork())
        {            
        }

        public static IEnumerable<EmployeeDataContract> ImportCsv(string filename)
        {
                var s = File.ReadAllLines(filename,Encoding.Default);
                return s.Select(EmployeeDataContract.Parse);
        }

        public void ExportCsv(string filepath)
        {
           var stream = File.OpenWrite(filepath);
           stream.SetLength(0);
           var file =  new StreamWriter (stream,Encoding.Default);
           foreach (var user in Table.All)
           {
              file.WriteLine(user);
           }
           file.Close();
        }

        protected override EmployeeDataContract ToCotract(Employee item)
        {
            return new EmployeeDataContract(item);
        }

        protected override Employee FromCotract(EmployeeDataContract item)
        {
            return new Employee(item);
        }
    }
}