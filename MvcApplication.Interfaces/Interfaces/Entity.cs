﻿using System.ComponentModel.DataAnnotations;

namespace MvcApplication.Interfaces
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
