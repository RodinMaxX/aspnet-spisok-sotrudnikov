﻿using System.Data.Entity;
using System.Linq;
using MvcApplication.Entities;
using MvcApplication.Interfaces;

namespace MvcApplication
{
    internal class Repository<T> : IRepository<T>
        where T : Entity
    {
        protected readonly EfDbContext Context;
        protected readonly DbSet<T> Table;

        public Repository(EfDbContext context)
        {
            Context = context;
            Table = context.Set<T>();
        }

        public IQueryable<T> All { get { return Table; } }

        public T Get(int id)
        {
            return Table.Find(id);
        }

        public void Delete(int id)
        {
            var old = Table.Find(id);
            Table.Remove(old);
            Context.SaveChanges();
        }

        public void Add(T item)
        {
            Table.Add(item);
            Context.SaveChanges();
        }

        public void Update(T item)
        {
            var old = Table.Find(item.Id);
            Context.Entry(old).CurrentValues.SetValues(item);
            Context.SaveChanges();
        }
    }
}