using System.Linq;
using System.Web.Mvc;
using MvcApplication.DataContract;
using MvcApplication.Entities;
using MvcApplication.Models;
using MvcApplication.Services;

namespace MvcApplication.Controllers
{
    public class GroupController : Controller
    {
        private readonly IGroupService _groups;
        private readonly IAccountService _accounts;

        public GroupController(IGroupService group, IAccountService accounts)
        {
            _groups = group;
            _accounts = accounts;
        }

        public ActionResult Index()
        {
            return View(_groups.WithAccountCount());
        }
       
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(EditGroupModel model, string isDeleted)
        {
            if (isDeleted != null)
            {
                if (_groups.InUsage(model.Group.Id)) return View("GroupInUsage");
                _groups.Delete(model.Group.Id);
            }
            else
            {
                /*
                if (!ModelState.IsValid)
                {
                    model.Group.Accounts = _groups.GetAccounts(model.Group.Id).ConvertAll(x => new Account(x));
                    model.AllAccounts = _accounts.GetAll();
                    return View(model);
                }
                 */
                var selected = model.Group.Accounts.Where(x => x.IsSelected != null).Select(x => x.Id);
                _groups.AddOrUpdate(new GroupDataContract(model.Group), selected.ToList());
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var group = (id == null) ? new GroupDataContract() : _groups.Get(id.Value);
            var model = new Group(group) {Accounts = _groups.GetAccounts(id).ConvertAll(x => new Account(x))};
            if (group != null) return View(new EditGroupModel { Group = model, AllAccounts = _accounts.GetAll() });
            return new HttpNotFoundResult("Нет категории с id = " + id);
        }
    }
}
