using System.Collections.Generic;
using System.ServiceModel;
using MvcApplication.DataContract;
using MvcApplication.Entities;

namespace MvcApplication.Services
{
    [ServiceContract]
    public interface ICartService
    {
        [OperationContract]
        string AddOrUpdate(int? employeeId, IList<CartItemDataContract> cartItems);
        
        [OperationContract]
        List<CartItemDataContract> Filter(int accountIdValue);

        [OperationContract]
        List<ProductDataContract> GetProducts(int accountId);
       
        [OperationContract]
        void FinishBuy(int? accountId);
    }
}